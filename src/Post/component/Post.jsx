import React from 'react';

const Post = (props) => (
  <div className="panel-body"> 
  { 
    props.postBody.map((postPart, idx)=>{
      return(<div>{postPart}</div>);
    })
  }
  </div> 
);
export default Post;